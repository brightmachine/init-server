#!/bin/bash

# relies upon .bash_script_helper.sh https://bitbucket.org/brightmachine/workspace/snippets/rXX8eb


Help()
{
   # Display Help
   echo "This script will 'switch' the git branch to a new upstream branch."
   echo
   echo "Syntax: switch.sh branch-name"
   echo
   echo "There must exist a directory called '.git' in the current directory."
   echo
   echo "Source from https://bitbucket.org/brightmachine/workspace/snippets/9XXb9g"
   echo
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${DIR}/.bash_script_helper.sh"

BRANCH=$1

if [[ ! $BRANCH || "$BRANCH" == "help" ]]; then
  Help
  exit;
fi

if [[ ! -d ".git" ]]; then
  Help
  error "No .git foler present in current directory."
  exit;
fi

CURRENT_BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`

echo
warn "You are checking out a new remote branch - 'upstream/$BRANCH':"
echo
echo_ WHITE UNDERLINE "1. No deployment is done with this script"
echo_ CYAN UNDERLINE "2. For any new dependencies or migrations, run 'deploy' when you are ready"
echo_ LYELLOW UNDERLINE "3. Your current branch (${CURRENT_BRANCH}) will be deleted locally (not from the server) after the branches are switched"
echo
echo
wantToContinue "Continue checking out upstream/$BRANCH" "y"

# remember, origin on the server is the same as upstream for us developers
git fetch --prune origin

git checkout -b "$BRANCH" "origin/$BRANCH"

wantToContinue "Delete your local '${CURRENT_BRANCH}' branch" "y"

git branch -d "${CURRENT_BRANCH}"