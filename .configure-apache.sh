#!/bin/bash

echo_ CYAN "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="

if ! ask "Configure Apache?" Y; then
    warn "🙈 Skipping Apache config"
    return
fi

echo_ CYAN "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="

APACHE_DIR="/etc/httpd"
APACHE_CONF="$APACHE_DIR/conf/httpd.conf"
APACHE_CONF_TMP="/tmp/httpd.conf"
APACHE_MODULES_CONF="$APACHE_DIR/conf.d/modules.conf"
APACHE_MODULES_CONF_TMP="/tmp/modules.conf"
APACHE_SSL_CONF="$APACHE_DIR/conf.d/ssl.conf"
APACHE_SSL_CONF_TMP="/tmp/ssl.conf"
APACHE_SSL_CERTS_DIR="/var/www/webroot/vhosts/ssl"
APACHE_UNUSED_FOLDERS_ROOT="/var/www"
APACHE_POEMS="/var/www/webroot/ROOT"

# check if apache dir exists
if [[ ! -f "$APACHE_CONF" ]]; then
  warn "Cannot find '$APACHE_CONF', skipping configuration"
  sleep 1
  return
fi

# configure apache conf
info "Attempting to configure Apache in $APACHE_DIR …"
sleep 1

if grep -qsc 'BM-CONFIG' "$APACHE_CONF"; then
  warn "🙈 BM config already added to '$APACHE_CONF', skipping configuration"
else
  info "Backing up $APACHE_CONF » $APACHE_CONF_TMP"
  cp "$APACHE_CONF" "$APACHE_CONF_TMP"

  PHP_VERSION=$(php -r 'echo PHP_MAJOR_VERSION.PHP_MINOR_VERSION;')
  SERVER_NODE_NAME=$(promptStudly "Provide the name for the server to go after JLS-BM- in the apache served-by header? E.g. CC-$PHP_VERSION")
  info "Using $SERVER_NODE_NAME as the server name"
  sed "s/NODENAME/$SERVER_NODE_NAME/" "${DIR}/stubs/httpd.conf" >> "$APACHE_CONF_TMP"
  sed -i "s/Jelastic autoconfiguration/Jelastic NON-autoconfiguration/" "$APACHE_CONF_TMP"
  sed -i "s/ServerAlias/#ServerAlias/" "$APACHE_CONF_TMP" # comment out catch-all server alias

  info "Restoring $APACHE_CONF « $APACHE_CONF_TMP"
  cp "$APACHE_CONF_TMP" "$APACHE_CONF"

  echo_ GREEN UNDERLINE "👍 Tweaked '$APACHE_CONF'"
fi

echo
sleep 1

# enable ssl module
info "Enabling Apache SSL modules in $APACHE_MODULES_CONF …"
sleep 1

info "Backing up $APACHE_MODULES_CONF » $APACHE_MODULES_CONF_TMP"
cp "$APACHE_MODULES_CONF" "$APACHE_MODULES_CONF_TMP"

sed -i "s/#LoadModule ssl_module/LoadModule ssl_module/" "$APACHE_MODULES_CONF_TMP"

info "Restoring $APACHE_MODULES_CONF « $APACHE_MODULES_CONF_TMP"
cp "$APACHE_MODULES_CONF_TMP" "$APACHE_MODULES_CONF"

echo_ GREEN UNDERLINE "👍 Enabled SSL modules in '$APACHE_MODULES_CONF'"

echo
sleep 1



# listen on port 443 and add default vhost for initial ssl handshake
info "Add default SSL vhost config in $APACHE_SSL_CONF …"
sleep 1

if [[ ! -d "${APACHE_SSL_CERTS_DIR}" ]]; then
  warn "🙈 Cannot find folder containing SSL certs at '$APACHE_SSL_CERTS_DIR', skipping creation of SSL vhost"
else

  cp "${DIR}/stubs/ssl.conf" "$APACHE_SSL_CONF_TMP"
  APACHE_SSL_CRT_FILE=$(ls "$APACHE_SSL_CERTS_DIR/"*.crt | head -1)
  APACHE_SSL_KEY_FILE=$(ls "$APACHE_SSL_CERTS_DIR/"*.key | head -1)
  sed -i "s|CRT_FILE|$APACHE_SSL_CRT_FILE|" "$APACHE_SSL_CONF_TMP"
  sed -i "s|KEY_FILE|$APACHE_SSL_KEY_FILE|" "$APACHE_SSL_CONF_TMP"

  info "Adding configured '$APACHE_SSL_CONF' …"
  cp "$APACHE_SSL_CONF_TMP" "$APACHE_SSL_CONF"

  echo_ GREEN UNDERLINE "👍 Added default SSL vhost in '$APACHE_SSL_CONF'"

fi

echo
sleep 1



# remove certain apache folders not needed
info "Attempting to clean up unused folders in Apache in $APACHE_UNUSED_FOLDERS_ROOT …"
sleep 1

rm -rf "$APACHE_UNUSED_FOLDERS_ROOT/cgi-bin/" "$APACHE_UNUSED_FOLDERS_ROOT/error/" "$APACHE_UNUSED_FOLDERS_ROOT/html/" "$APACHE_UNUSED_FOLDERS_ROOT/icons/" "$APACHE_UNUSED_FOLDERS_ROOT/noindex/" "$APACHE_UNUSED_FOLDERS_ROOT/server-status/"
echo_ GREEN UNDERLINE "👍 Cleaned up unused folders in '$APACHE_UNUSED_FOLDERS_ROOT'"

echo
sleep 1


# add default poems to ROOT
info "Adding default poems to $APACHE_POEMS …"
sleep 1

cp "$DIR/stubs/index.php" "$APACHE_POEMS/"
echo_ GREEN UNDERLINE "👍 Added poems to '$APACHE_POEMS'"

echo
echo
sleep 1