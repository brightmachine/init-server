#!/bin/bash

echo_ CYAN "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="

if ! ask "Configure PHP?" Y; then
    warn "🙈 Skipping PHP config"
    return
fi

echo_ CYAN "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="


PHP_CONF="/etc/php.ini"
PHP_CONF_TMP="/tmp/php.ini"

info "Attempting to configure PHP in $PHP_CONF …"
echo


if [[ ! -f "$PHP_CONF" ]]; then
  echo_ GREEN UNDERLINE "🙈 No file at '$PHP_CONF', skipping configuration"
  return
fi

# on JLS, we cannot sed a file, due to permissions. Instead, we need to take a copy, make changes, and copy it back
cp "$PHP_CONF" "$PHP_CONF_TMP"

# enable intl extension
sed -i "s/;extension=intl.so/extension=intl.so/" "$PHP_CONF_TMP"

cp "$PHP_CONF_TMP" "$PHP_CONF"

echo_ GREEN UNDERLINE "👍 PHP config updated"

echo
echo
sleep 1