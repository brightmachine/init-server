This repo is used to configure a new Apache/PHP environment on Jelastic.

**NB:** before running this script, you should ensure that you have mounted a separate storage drive to `webroot/` and have the following folders:

1. `webroot/sites/`
2. `webroot/vhosts`
3. `webroot/vhosts/ssl` - containing at least 1 .crt and 1 .key file 

To kick off the install, SSH into the new server and run:

```bash
curl -sS https://bitbucket.org/brightmachine/init-server/raw/master/init | bash && source /var/www/.profile
```
