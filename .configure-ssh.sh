#!/bin/bash

echo_ CYAN "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="

if ! ask "Configure SSH keys?" Y; then
    warn "🙈 Skipping SSH key config"
    return
fi

echo_ CYAN "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="


SSH_DIR="/var/www/.ssh"

info "Attempting to configure SSH on the server …"
sleep 1

if [[ -f "$SSH_DIR/id_rsa" ]]; then
  echo_ GREEN UNDERLINE "🙈 Already have an SSH key at '$SSH_DIR/id_rsa', skipping key generation"
else
  # add ssh keys unless already present
  ssh-keygen -q -N '' -f "$SSH_DIR/id_rsa" > /dev/null
  echo_ GREEN UNDERLINE "👍 Generated SSH key at '$SSH_DIR/id_rsa'"
  echo
  echo
  SSH_PUB=$(cat "$SSH_DIR/id_rsa.pub")
  echo "$SSH_PUB"
  echo
  info "Above is your public SSH key. Log into bitbucket as 'deploy' and add the above as a new key:"
  echo "https://bitbucket.org/account/settings/ssh-keys/"
  echo
fi

echo

if ask "Test your bitbucket connection?" Y; then
    echo
    if ssh -T git@bitbucket.org; then
      echo_ GREEN UNDERLINE "👍 SSH connection to bitbucket works"
    else
      echo_ RED UNDERLINE "💩 SSH connection to bitbucket is not authorised"
    fi
else
  echo
  echo_ GREEN UNDERLINE "👍 SSH setup complete"
fi

echo
echo
sleep 1