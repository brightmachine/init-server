#!/bin/bash

Help()
{
   # Display Help
   echo "This script will initialise a new JLS environment:"
   echo
   echo "1. install a bin/ directory with composer, lockrun (for brightcms) and a switch.sh command for switching branches"
   echo "2. configure Apache to server from webroot/vhosts/"
   echo "3. ensure the SSL module is enabled"
   echo "4. add a ~/.profile file to set-up various paths"
   echo
}

ARG=$1

if [[ "$ARG" == "help" ]]; then
  Help
  exit;
fi
