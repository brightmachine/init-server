COMPOSER_HOME="/var/www/.composer"
export PATH=$PATH:$COMPOSER_HOME/vendor/bin
export PATH=$PATH:bin:vendor/bin

alias deploy="bin/deploy.sh --no-dev"
alias c="composer"
alias art="php artisan"
