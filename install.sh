#!/bin/bash

Help()
{
   # Display Help
   echo "This script will initialise a new JLS environment:"
   echo
   echo "1. install a bin/ directory with composer, lockrun (for brightcms) and a switch.sh command for switching branches"
   echo "2. configure Apache to serve from webroot/vhosts/"
   echo "3. ensure the SSL module is enabled"
   echo "4. add a ~/.profile file to set-up various paths"
   echo "5. ensure we have an ssh key to add"
   echo "6. configure PHP extensions"
   echo
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${DIR}/bin/.bash_script_helper.sh"

source "${DIR}/.help.sh"



source "${DIR}/.configure-server.sh"

source "${DIR}/.configure-apache.sh"

source "${DIR}/.configure-php.sh"

source "${DIR}/.configure-ssh.sh"

echo_ CYAN "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="

sleep 1

if ask "Restart apache?" Y; then
  info "Ok, restarting apache"
  sudo service httpd restart
else
  info "No worries."
fi

echo
echo_ CYAN "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
echo
echo_ LIGHTGREEN "👍 Server initialisation complete."
echo
sleep 1