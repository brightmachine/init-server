#!/bin/bash

echo_ CYAN "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="

if ! ask "Configure server by adding binaries, composer & .profile?" Y; then
    warn "🙈 Skipping server setup"
    return
fi

echo_ CYAN "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="


BIN_DIR="/var/www/bin"
DOT_PROFILE="/var/www/.profile"

info "Attempting to install binaries into $BIN_DIR folder …"
sleep 1

if [[ -d "${BIN_DIR}" ]]; then
  warn "🙈 The folder at '$BIN_DIR' already exists, skipping creation"
else
  mkdir "$BIN_DIR"
fi

cp "${DIR}/bin/"* "$BIN_DIR/"
echo_ GREEN UNDERLINE "👍 Binaries installed in $BIN_DIR/"
sleep 1
echo
echo

info "Attempting to install composer into $BIN_DIR …"
sleep 1

if [[ -f "$BIN_DIR/composer" ]]; then

  warn "Composer already installed."

  if ask "Update composer to latest?" N; then
    "$BIN_DIR/composer" self-update
    echo_ GREEN UNDERLINE "👍 composer updated"
  else
    echo_ GREEN UNDERLINE "🙈 skipping composer update"
  fi
else
  curl -sS https://getcomposer.org/installer | php && mv composer.phar "$BIN_DIR/composer"
  echo_ GREEN UNDERLINE "👍 composer installed"
fi
echo
echo
sleep 1

if ask "Do you want to provide a github oauth code for composer?" Y; then
    echo
    info "See https://getcomposer.org/doc/articles/authentication-for-private-packages.md#github-oauth"
    echo
    sleep 1

    GITHUB_OAUTH_CODE=$(promptAnyValue "Provide the github personal access token?")

    if composer config -g github-oauth.github.com "$GITHUB_OAUTH_CODE"; then
      echo_ GREEN UNDERLINE "👍 Github oauth token set for composer"
    else
      echo_ RED UNDERLINE "💩 Could not set Github oauth token for composer"
    fi
else
  echo
  echo_ GREEN UNDERLINE "🙈 skipping github oauth composer"
fi
echo
echo
sleep 1

info "Attempting to install .profile …"
sleep 1

if [[ -f "$DOT_PROFILE" ]]; then

  warn "The file '$DOT_PROFILE' already exists"

  if ask "Do you want to override it?" N; then
    cp "${DIR}/stubs/.profile" "$DOT_PROFILE"
    source "$DOT_PROFILE"
    echo_ GREEN UNDERLINE "👍 'stubs/.profile' overwritten at '$DOT_PROFILE'"
  else
    echo_ GREEN UNDERLINE "🙈 Skipping installation of .profile"
  fi
else
  cp "${DIR}/stubs/.profile" "$DOT_PROFILE"
  source "$DOT_PROFILE"
  echo_ GREEN UNDERLINE "👍 'stubs/.profile' written to '$DOT_PROFILE'"
fi

echo
echo
sleep 1